import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../Glob.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        color: Glob.background,
        child: Column(
          children: [
            Container(
              height: 50,
              width: 50,
              child: SvgPicture.asset(

                'assets/loading.svg',
              ),
            ),
            SizedBox(
              height: 4,
            ),
             Text(
                'Loading...',
                style: TextStyle(color: Glob.primaryOne),
              ),

          ],
        ),

    );
  }
}

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import '../Utility/Utility.dart';

class CustomTextField extends StatelessWidget {
  final String _hint;
  final Function _function;

  CustomTextField(this._hint, this._function);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Utility.getScreenW(context) * 0.83,
      margin: const EdgeInsets.all(7),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(9),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 3.0),
        child: TextField(
          textDirection: ui.TextDirection.rtl,
          onChanged: (str) {
            _function(str);
          },
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: _hint,
            hintTextDirection: ui.TextDirection.rtl,
          ),
        ),
      ),
    );
  }
}

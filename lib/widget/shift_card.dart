import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

import '../Utility/Utility.dart';
import '../Glob.dart';
import '../models/Shift.dart';

class ShiftCard extends StatelessWidget {
  final Shift shift;
  final Map<String, Map<Function, Color>> functions;

  ShiftCard(this.shift, this.functions);

  @override
  Widget Divider() {
    return Container(
      width: double.infinity,
      height: 1,
      margin: const EdgeInsets.only(
        bottom: 8,
        top: 4,
      ),
      color: Colors.grey[400],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Utility.getScreenW(context) * 0.85,
      margin: const EdgeInsets.only(right: 20,),
      child: Card(
        shadowColor: Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          shift.pharmacyName ?? 'no pharmacy name',
                          style: TextStyle(
                            color: Glob.accentColor,
                            fontSize: 18,
                            // height: 0.9,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Divider(),
                        Text(
                         shift.state.isNotEmpty && shift.city.isNotEmpty ? 'استان : ${shift.state}       شهر : ${shift.city}' : 'no location description... ',
                          style: TextStyle(height: 0.9),
                        ),
                        Divider(),
                        Text(
                          '${shift.exactAddress} : ادرس' ?? 'no address',
                          textAlign: TextAlign.right,
                          style: TextStyle(height: 0.95),
                        ),
                        Divider(),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                 shift.start == null ? 'نامشخص' : 'تاریخ شروع : ${DateFormat('yyyy/MM/dd').format(shift.start)}',
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  shift.start == null ? 'نامشخص' : 'تاریخ پایان : ${DateFormat('yyyy/MM/dd').format(shift.end)}',
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                          ],
                        ),
                        // Text(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: SvgPicture.asset('assets/dummy.svg'),
                        width: 50,
                        height: 50,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Container(
                        width: 50,
                        child: Text(
                          shift.creatorIdentityServer.fullName ?? 'بدون نام',
                          maxLines: 3,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            height: 1,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              height: 1,
              color: Colors.grey[400],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal:20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        for (int i = 0; i < functions.length; i++)
                          Row(
                            children: [
                              GestureDetector(
                                onTap: functions.values
                                    .toList()[i]
                                    .keys
                                    .toList()[0],
                                child: Container(
                                  height: 26,
                                  decoration: BoxDecoration(
                                    color: functions.values
                                        .toList()[i]
                                        .values
                                        .toList()[0],
                                    borderRadius: BorderRadius.circular(13),
                                  ),
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                  alignment: Alignment.center,
                                  child: Text(
                                    functions.keys.toList()[i],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(height: 0.9, color: Colors.white),
                                  ),
                                ),
                              ),
                              SizedBox(width: 4),
                            ],
                          ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(shift.price == null ? 'مبلغ نامشخص' :'مبلغ : ${shift.price}'),
                        Icon(
                          Icons.circle,
                          color: Glob.primaryOne,
                          size: 10,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 14,
              decoration: BoxDecoration(
                color: Colors.grey[400],
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

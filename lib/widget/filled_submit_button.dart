import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Glob.dart';
import '../Utility/Utility.dart';

class FilledSubmitButton extends StatelessWidget {
  final String _text;
  final Function _function;
  FilledSubmitButton(this._text, this._function);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _function,
      child: Container(
        alignment: AlignmentDirectional.center,
        width: Utility.getScreenW(context) * 0.85,
        padding: const EdgeInsets.symmetric(vertical: 12),
        decoration: BoxDecoration(
          color: Glob.primaryOne,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          _text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

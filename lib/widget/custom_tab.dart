import 'package:flutter/material.dart';

import '../Glob.dart';

import '../Utility/Utility.dart';

class CustomTab extends StatefulWidget {
  final Widget top;
  final List<String> titles;
  final List<Widget> body;
  final int initPage;

  CustomTab({this.top, this.body, this.titles, this.initPage = 0});

  @override
  _CustomTabState createState() => _CustomTabState();
}

class _CustomTabState extends State<CustomTab> {
  int _selectedTab;
  PageController _pageController;

  void onTabChange(int current) {
    setState(() {
      _selectedTab = current;
      _pageController.animateToPage(
        current,
        duration: Duration(milliseconds: 800),
        curve: Curves.fastLinearToSlowEaseIn,
      );
    });
  }

  @override
  void initState() {
    _pageController = PageController(initialPage: widget.initPage);
    _selectedTab = widget.initPage;
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (_selectedTab == 2) widget.top,
        Container(
          color: Glob.backgroundDart,
          child: Row(
            children: <Widget>[
              Tab(
                title: widget.titles[0] ?? 'title',
                tabIndex: 0,
                selectedTab: _selectedTab,
                onPress: () {
                  onTabChange(0);
                },
              ),
              Tab(
                title: widget.titles[1] ?? 'title',
                tabIndex: 1,
                selectedTab: _selectedTab,
                onPress: () {
                  onTabChange(1);
                },
              ),
              Tab(
                title: widget.titles[2] ?? 'title',
                tabIndex: 2,
                selectedTab: _selectedTab,
                onPress: () {
                  onTabChange(2);
                },
              ),
            ],
          ),
        ),
        Expanded(
          child: PageView(
            onPageChanged: (i) {
              setState(() {
                _selectedTab = i;
              });
            },
            controller: _pageController,
            children: widget.body,
            // reverse: true,
          ),
        ),
      ],
    );
  }
}

class Tab extends StatelessWidget {
  final String title;
  final int tabIndex;
  final int selectedTab;
  final Function onPress;

  Tab({this.title, this.selectedTab, this.tabIndex, this.onPress});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onPress,
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 3, vertical: 5),
          color: Glob.background,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                ),
                child: Text(
                  title ?? 'تب جدید',
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                height: 4,
                color:
                    selectedTab == tabIndex ? Glob.primaryOne : Glob.background,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

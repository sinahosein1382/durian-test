import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

import '../Utility/Utility.dart';

class DateTimePicker extends StatelessWidget {
  final Function function;
  DateTime _endDateTime;
  DateTime _startDateTime;

  DateTimePicker(this.function);

  @override
  Widget build(BuildContext context) {
    DateTime _endDate;
    TimeOfDay _endTime;
    DateTime _startDate;
    TimeOfDay _startTime;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            PickerButton('ساعت', 'assets/clock.svg', () async {
              TimeOfDay timeOfDay = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.now(),
                  initialEntryMode: TimePickerEntryMode.input,
                  builder: (context, child) {
                    return MediaQuery(
                        data: MediaQuery.of(context).copyWith(
                            // Using 24-Hour format
                            alwaysUse24HourFormat: true),
                        child: child);
                  });
              _startTime = timeOfDay;
              if(_endDate != null && _endTime != null && _startDate != null && _startTime != null){
                _endDateTime = DateTime(_endDate.year,_endDate.month, _endDate.day, _endTime.hour, _endTime.minute );
                _startDateTime = DateTime(_startDate.year,_startDate.month, _startDate.day, _startTime.hour, _startTime.minute );
                function(_endDateTime, _startDateTime);
              }
            }),
            PickerButton(
              'تاریخ شروع',
              'assets/calendar.svg',
              () async {
                DateTime dateTime = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now(),
                  lastDate: DateTime(DateTime.now().year + 2),
                );
                _startDate =
                    DateTime(dateTime.year, dateTime.month, dateTime.day);

                if(_endDate != null && _endTime != null && _startDate != null && _startTime != null){
                  _endDateTime = DateTime(_endDate.year,_endDate.month, _endDate.day, _endTime.hour, _endTime.minute );
                  _startDateTime = DateTime(_startDate.year,_startDate.month, _startDate.day, _startTime.hour, _startTime.minute );
                  function(_endDateTime, _startDateTime);
                }

              },
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            PickerButton(
              'ساعت',
              'assets/clock.svg',
              () async {
                TimeOfDay timeOfDay = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.now(),
                    initialEntryMode: TimePickerEntryMode.input,
                    builder: (context, child) {
                      return MediaQuery(
                          data: MediaQuery.of(context).copyWith(
                              // Using 24-Hour format
                              alwaysUse24HourFormat: true),
                          child: child);
                    });
                _endTime = timeOfDay;

                if(_endDate != null && _endTime != null && _startDate != null && _startTime != null){
                  _endDateTime = DateTime(_endDate.year,_endDate.month, _endDate.day, _endTime.hour, _endTime.minute );
                  _startDateTime = DateTime(_startDate.year,_startDate.month, _startDate.day, _startTime.hour, _startTime.minute );
                  function(_endDateTime, _startDateTime);
                }
              },
            ),
            PickerButton(
              'تاریخ پایان',
              'assets/calendar.svg',
              () async {
                DateTime dateTime = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now(),
                  lastDate: DateTime(DateTime.now().year + 2),
                );
                _endDate =
                    DateTime(dateTime.year, dateTime.month, dateTime.day);

                if(_endDate != null && _endTime != null && _startDate != null && _startTime != null){
                  _endDateTime = DateTime(_endDate.year,_endDate.month, _endDate.day, _endTime.hour, _endTime.minute );
                  _startDateTime = DateTime(_startDate.year,_startDate.month, _startDate.day, _startTime.hour, _startTime.minute );
                  function(_endDateTime, _startDateTime);
                }
              },
            ),
          ],
        ),
      ],
    );
  }
}

class PickerButton extends StatelessWidget {
  final Function _onTap;
  final String _svg;
  final String _title;

  PickerButton(this._title, this._svg, this._onTap);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onTap,
      child: Container(
        width: Utility.getScreenW(context) * 0.37,
        padding: const EdgeInsets.all(9),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(
              Icons.arrow_back_ios,
              color: Colors.grey,
            ),
            Text(_title),
            Container(
              height: 28,
              width: 28,
              child: SvgPicture.asset(_svg),
            ),
          ],
        ),
      ),
    );
  }
}

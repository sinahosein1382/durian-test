import 'package:durian_test/Utility/Utility.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:carousel_slider/carousel_slider.dart';
import '../Glob.dart';

class CustomCrouselSlider extends StatefulWidget {
  @override
  _CustomCrouselSliderState createState() => _CustomCrouselSliderState();
}

class _CustomCrouselSliderState extends State<CustomCrouselSlider> {
  CarouselController controller = new CarouselController();
  int index = 0;
  @override
  Widget build(BuildContext context) {
    List<Widget> items = [
      item(context),
      item(context),
      item(context),
    ];


    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Column(
        children: [
          CarouselSlider(

            carouselController: controller,

            options: CarouselOptions(
                height: 200,
                initialPage: 0,
                autoPlay: true,


                onPageChanged: (i, _) {
                 setState(() {
                   print(i);
                   index = i;
                 });
                }),
            items: items,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ...items
                    .map(
                      (e) => Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Icon(
                          Icons.circle,
                          size: 10,
                          color: index == items.indexOf(e) ? Glob.primaryOne : Colors.grey[400],
                        ),
                      ),
                    )
                    .toList()
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget item(BuildContext context) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    width: Utility.getScreenW(context) * 0.9,
    decoration: BoxDecoration(
      color: Colors.grey[400],
      borderRadius: BorderRadius.circular(22),
    ),
  );
}

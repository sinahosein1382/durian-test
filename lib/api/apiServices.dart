import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import 'package:durian_test/models/Shift.dart';
import '../Glob.dart';

class ApiServices {
  Future<List<Shift>> getShiftReplacements() async {
    Uri uurl = Uri.parse(
        '${Glob.protocol}shiftreplace.${Glob.domain}/api/user/shift/GetPagination');
    var body = json.encode({"Page": 1, "PageSize": 5, "Filter": {}});
    try {
      var response = await http.post(uurl, body: body, headers: {
        'Authorization': 'Bearer ${Glob.token}',
        "Content-Type": 'application/json',
      });
      var mapResponse = json.decode(response.body);
      // print(mapResponse);
      List<dynamic> data = mapResponse['data']['data'];
      // if(data == [] && data == null){
      //   throw Exception('no shift');
      // }
      List<Shift> list = data.map(
        (dynamic e) {
          return Shift(
            id: e['id'] as int,
            state: e['state'],
            city: e['city'],
            exactAddress: e['exactAddress'],
            pharmacyName: e['pharmacyName'],
            price: e['price'] as double,
            start: DateFormat('dd/MM/yyyy HH:mm').parse(e['start']),
            end: DateFormat('dd/MM/yyyy HH:mm').parse(e['end']),
            creatorIdentityServer: CreatorIdentityServer(
              id: e['creatorIdentityServer']['id'],
              userName: e['creatorIdentityServer']['username'],
              fullName: e['creatorIdentityServer']['fullName'],
              image: e['creatorIdentityServer']['image'],
            ),
          );
          // list.add(shift);
        },
      ).toList();
      print('after adding ${list.length}');
      return list;
    } catch (e) {
      print(e);
    }
    return null;
  }
}

import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../Utility/Utility.dart';
import '../Glob.dart';
import '../models/Shift.dart';
import '../BLoC/shiftBloc.dart';
import '../widget/search_bar.dart';
import '../widget/custom_tab.dart';
import '../widget/shift_card.dart';
import '../widget/custom_crousel_slider.dart';
import '../widget/loading_widget.dart';
import '../widget/city_state_picker.dart';
import '../widget/date_time_picker.dart';
import '../widget/custom_text_field.dart';
import '../widget/filled_submit_button.dart';

class ShiftMainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Utility.getScreenW(context),
      height: Utility.getScreenH(context),
      color: Glob.background,
      child: CustomTab(
        initPage: 2,
        titles: [
          'مدیریت شیفت',
          'ثبت شیفت',
          'شیفت ها',
        ],
        top: SearchBar(),
        body: [
          ShiftManagement(),
          ShiftSubmit(),
          Shifts(),
        ],
      ),
    );
  }
}

class ShiftSubmit extends StatefulWidget {
  @override
  _ShiftSubmitState createState() => _ShiftSubmitState();
}

class _ShiftSubmitState extends State<ShiftSubmit> {
  Shift shift;

  @override
  void initState() {
    shift = new Shift();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 6,
            ),
            CityStatePicker((state, city) {
              shift.state = state;
              shift.city = city;
            }),
            SizedBox(
              height: 7,
            ),
            CustomTextField('نام داروخانه', (value) {
              shift.pharmacyName = value;
            }),
            SizedBox(
              height: 7,
            ),
            DateTimePicker((DateTime end, DateTime start) {
              shift.start = start;
              shift.end = end;
            }),
            SizedBox(
              height: 7,
            ),
            CustomTextField('مبلغ', (value) {
              shift.price = double.parse(value);
            }),
            CustomTextField('آدرس', (value) {
              shift.exactAddress = value;
            }),
            CustomTextField('توضیحات', (value) {
              shift.detail = value;
            }),
            SizedBox(
              height: 9,
            ),
            FilledSubmitButton('ثبت', () {
              if (shift.city == null || shift.state == null ||
                  shift.detail == null || shift.exactAddress == null ||
                  shift.price == null || shift.pharmacyName == null ||
                  shift.end == null || shift.start == null ){
                return;
              }
              
            }),
          ],
        ),
      ),
    );
  }
}

class Shifts extends StatelessWidget {
  ShiftBloc shiftBloc = new ShiftBloc();
  final Color primary = Glob.primaryOne;
  List<Shift> list = new List<Shift>();

  @override
  Widget build(BuildContext context) {
    shiftBloc.getPagination();
    return Column(
      children: [
        CustomCrouselSlider(),
        StreamBuilder(
          stream: shiftBloc.shiftsStream,
          builder: (context, snapshot) {
            list = snapshot.data;
            if (snapshot.hasData) {
              return Container(
                width: Utility.getScreenW(context),
                height: 225,
                padding: const EdgeInsets.all(8),
                child: list.length == 0
                    ? Center(
                  child: Text(
                    'No shifts',
                    style: TextStyle(
                      fontSize: 18,
                      color: Glob.primaryOne,
                    ),
                  ),
                )
                    : ListView.builder(
                  scrollDirection: Axis.horizontal,
                  reverse: true,
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return ShiftCard(
                      list[index],
                      {
                        'گفتگو': {() {}: Glob.primaryOne},
                        ' اعلام امادگی': {() {}: Colors.grey},
                      },
                    );
                  },
                ),
              );
            } else {
              return Container(
                width: Utility.getScreenW(context),
                height: 225,
                padding: const EdgeInsets.all(8),
                child: LoadingWidget(),
              );
            }
          },
        ),
      ],
    );
  }
}

class ShiftManagement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('shift management'),
    );
  }
}

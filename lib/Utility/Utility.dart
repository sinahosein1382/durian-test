import 'package:flutter/cupertino.dart';


class Utility{
  static double _screenW;
  static double _screenH;

  static double getScreenH(BuildContext context){
    return MediaQuery.of(context).size.height;
  }
  static double getScreenW(BuildContext context){
    return MediaQuery.of(context).size.width;
  }
}
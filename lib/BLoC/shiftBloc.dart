import 'dart:async';

import '../models/Shift.dart';
import '../api/apiServices.dart';

class ShiftBloc {
  ApiServices api = new ApiServices();
  List<Shift> list = new List<Shift>();
  StreamController _streamController;


  StreamSink<List<Shift>> get shiftsSink => _streamController.sink;
  Stream<List<Shift>> get shiftsStream => _streamController.stream;

  ShiftBloc(){
    _streamController = new StreamController<List<Shift>>.broadcast();
    api = new ApiServices();
  }

  getPagination() async{
    try{
      list = await api.getShiftReplacements();
     shiftsSink.add(list);
     print('\n');
     print('---------------------------------------- 000000000000000000000 $list \n\n\n ');
    }catch(e){
      print(e);

    }
  }
}
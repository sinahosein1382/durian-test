//represent a shift replacement
import 'package:flutter/cupertino.dart';

class Shift {
  int id;
  String state;
  String city;
  String exactAddress;
  String pharmacyName;
  double price;
  DateTime start;
  DateTime end;
  String detail;
  CreatorIdentityServer creatorIdentityServer;

  Shift({
    this.id,
    this.state,
    this.city,
    this.exactAddress,
    this.pharmacyName,
    this.price,
    this.start,
    this.end,
    this.creatorIdentityServer,
    this.detail,
  });
}

//represent the shift replacement creator
class CreatorIdentityServer {
  String id;
  String userName;
  String fullName;
  String image;

  CreatorIdentityServer({
    this.id,
    this.userName,
    this.fullName,
    this.image,
  });
}
